
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('base.urls')),
    path('', include('story1.urls')),
    path('', include('mataKuliah.urls')),
    path('', include('story7.urls')),
    path('', include('story8.urls')),
    path('', include('story9.urls')),
]
