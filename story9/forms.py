from django.contrib.auth.models import User
from django import forms



class RegisterForm(forms.Form):
    username = forms.CharField(
        label="Username ",
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'example: Rasyid',
                'name': 'username',
            }
        ),
        max_length=16
    )

    email = forms.EmailField(
        label="Email ",
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'name': 'email',
            }
        )
    )

    password1 = forms.CharField(
        label="Password ",
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'name': 'password',
            }
        )
    )




class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=16,
        label="Username: ",
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'username',
            }
        )
    )

    password = forms.CharField(
        label="Password: ",
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'name': 'username',
            }
        )
    )
