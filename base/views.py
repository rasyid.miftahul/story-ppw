from django.shortcuts import render

def index(request):
    content = {
        "title" : "Rasyid's Page"
    }
    return render(request, 'base/index.html', content)

def aboutme(request):
    content = {
        "title" : "About Rasyid"
    }
    return render(request, 'base/aboutme.html', content)