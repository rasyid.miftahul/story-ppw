$("#keyword").keyup(function () {
    var isi = $("#keyword").val();
    var url_google_apis = "/data?q=" + isi;
    $.ajax({
        url: url_google_apis,
        success: function (hasil) {
            var obj_hasil = $("#hasil");
            obj_hasil.empty();
            
            for (index = 0; index < hasil.items.length; index++) {
                var tmp_title = hasil.items[index].volumeInfo.title;
                var tmp_image = hasil.items[index].volumeInfo.imageLinks.smallThumbnail;
                var tmp_description = hasil.items[index].volumeInfo.description;
                var tmp_more_on = hasil.items[index].volumeInfo.previewLink;
                var final_image = "<img src=" + tmp_image + "class=" + "card-img" + ">"
                obj_hasil.append(
                    "<div class=" + "card mb-3" + "style=" + "max-width: 540px;" + ">" +
                        "<div class=" + "row no-gutters" + ">" +
                            "<div class=" + "col-md-4" + " style='margin: auto; display: flex'>" +
                                final_image +
                            "</div>" +
                            "<div class=" + "col-md-8" + ">" +
                                "<div class=" + "card-body" + ">" +
                                    "<h5 class=" + "card-title" + ">" + tmp_title + "</h5>" +
                                    "<p class=" + "card-text" +">" + tmp_description + "</p>" +
                                    "<a tar get='_blank' href=" + tmp_more_on + "class=" + "btn btn-primary" + " style='color: black;'>" + "Read More" + "</a>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</div>"
                );
            }
        }
    });
});
