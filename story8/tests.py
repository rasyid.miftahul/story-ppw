from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import indexx
from .apps import Story8Config
from django.apps import apps

# Create your tests here.

class UrlsTest8(TestCase):
    def setUp(self):
        self.index = reverse('indexx')
    
    def test_index_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, indexx)

class ViewsTest8(TestCase):
    def setUp(self):
        self.client = Client()
        self.index = reverse("indexx")
    
    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8/index.html')

class TestSearchEvent(TestCase):
    def test_event_searchbook_url_is_exist(self):
        response = Client().get('/data/?q=book')
        self.assertEqual(response.status_code, 200)



class TestApp8(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')