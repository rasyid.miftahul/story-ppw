from django.urls import include, path
from . import views

urlpatterns = [
    path('story8/', views.indexx, name='indexx'),
    path('data/', views.caribuku, name='caribuku')
]