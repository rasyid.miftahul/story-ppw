from django.urls import include, path
from . import views
app_name = 'mataKuliah'

urlpatterns = [
    path('mataKuliah/create/', views.create, name="create"),
    path('mataKuliah/detail/<int:input_id>', views.detail, name="detail"),
    path('mataKuliah/delete/<int:input_id>', views.delete, name="delete"),
    path('mataKuliah/', views.show)
]