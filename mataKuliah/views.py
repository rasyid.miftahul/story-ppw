from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.core.checks import messages
from django.http import HttpResponseRedirect
from .forms import MatkulForm
from .models import MatkulModel



def show(request):
    matkul = MatkulModel.objects.all()

    content = {
        "title" : "Daftar Mata Kuliah",
        "matkuls" : matkul
    }   
    return render(request, 'mataKuliah/show.html', content)

def detail(request, input_id):
    matkul = MatkulModel.objects.get(id=input_id)

    content = {
        "title" : "List Matkul",
        "matkul" : matkul
    }
    return render(request, 'mataKuliah/detail.html', content)
    

def create(request):
    matkul_form = MatkulForm(request.POST or None)

    if request.method == 'POST':
        if matkul_form.is_valid():
            matkul_form.save()
            return HttpResponseRedirect("/mataKuliah")

    content = {
        "title" : "Input Matkul",
        "matkul_form" : matkul_form,
    }
    return render(request, 'mataKuliah/create.html', content)

def delete(request, input_id):
    matkul = get_object_or_404(MatkulModel, id=input_id)
    matkul.delete()
    
    content = {
        "title":"Delete Matkul",
        "matkul" : matkul
    }
    return render(request, 'mataKuliah/delete.html', content)

