from django.db import models
from django.core.exceptions import  ValidationError


class MatkulModel(models.Model):
    nama_matkul = models.CharField(max_length=20)
    dosen = models.CharField(max_length=30)
    LIST_SKS = (
        ('7','7'),
        ('6','6'),
        ('5','5'),
        ('4','4'),
        ('3','3'),
        ('2','2'),
        ('1','1'),
    )
    sks = models.CharField(max_length=1, choices=LIST_SKS, default='1')
    ruangan = models.CharField(max_length=10)
    semester = models.CharField(max_length=30)
    desc_matkul = models.TextField()

    def __str__(self):
        return f"Mata Kuliah {self.nama_matkul} Semester {self.semester}"
