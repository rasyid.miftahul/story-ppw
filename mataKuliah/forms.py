from django import forms
from .models import MatkulModel

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MatkulModel
        fields = [
            'nama_matkul',
            'dosen',
            'sks',
            'ruangan',
            'semester',
            'desc_matkul',
        ]


