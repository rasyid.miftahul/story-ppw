from django.shortcuts import render

def index(request):
    return render(request, 'story1/index.html')
