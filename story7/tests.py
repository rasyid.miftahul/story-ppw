from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import index
from .apps import Story7Config
from django.apps import apps

# Create your tests here.

class UrlsTest(TestCase):
    def setUp(self):
        self.index = reverse('index')
    
    def test_index_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.index = reverse("index")
    
    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story7/index.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')
